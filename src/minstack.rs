use std::cmp;

pub struct MinStack<T> {
    items: Vec<T>,
    seq_mins: Vec<T>,
}

impl<T> MinStack<T> where T: Ord + Clone {
    pub fn new() -> MinStack<T> {
        MinStack {
            items: Vec::new(),
            seq_mins: Vec::new()
        }
    }

    pub fn push(&mut self, item: T) {
        self.items.push(item.clone());
        let current_min = self.seq_mins.last().cloned();
        match current_min {
            Some(a) => self.seq_mins.push(cmp::min(a, item)),
            None => self.seq_mins.push(item),
        }
    }

    pub fn pop(&mut self) -> Option<T> {
        self.seq_mins.pop();
        self.items.pop()
    }

    pub fn min(&self) -> Option<&T> {
        self.seq_mins.last()
    }

    pub fn is_empty(&self) -> bool {
        self.items.is_empty()
    }
}

#[cfg(test)]
mod tests {
    use super::MinStack;

    #[test]
    fn contents_during_push() {
        let mut stack = MinStack::<usize>::new();
        stack.push(3);
        stack.push(2);
        stack.push(19);
        stack.push(7);
        stack.push(0);
        stack.push(12);
        assert_eq!(stack.items, [3, 2, 19, 7, 0, 12]);
        assert_eq!(stack.seq_mins, [3, 2, 2, 2, 0, 0]);
    }

    #[test]
    fn min_during_pop() {
        let mut stack = MinStack::<usize>::new();
        stack.push(3);
        stack.push(2);
        stack.push(7);
        stack.push(0);

        assert_eq!(stack.min().unwrap(), &0);
        assert_eq!(stack.pop(), Some(0));
        assert_eq!(stack.min().unwrap(), &2);
        assert_eq!(stack.pop(), Some(7));
        assert_eq!(stack.min().unwrap(), &2);
        assert_eq!(stack.pop(), Some(2));
        assert_eq!(stack.min().unwrap(), &3);
        assert_eq!(stack.pop(), Some(3));
        assert_eq!(stack.min(), None);
        assert!(stack.is_empty());
    }
}

