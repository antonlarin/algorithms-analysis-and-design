use minstack::MinStack;


pub struct MinQueue<T> {
    pushing_stack: MinStack<T>,
    popping_stack: MinStack<T>,
}

impl<T> MinQueue<T> where T: Ord + Clone {
    pub fn new() -> MinQueue<T> {
        MinQueue {
            pushing_stack: MinStack::new(),
            popping_stack: MinStack::new(),
        }
    }

    pub fn is_empty(&self) -> bool {
        self.pushing_stack.is_empty() && self.popping_stack.is_empty()
    }

    pub fn push(&mut self, item: T) {
        self.pushing_stack.push(item)
    }

    pub fn min(&self) -> Option<&T> {
        match (self.pushing_stack.min(), self.popping_stack.min()) {
            (None, None) => None,
            (None, min) | (min, None) => min,
            (Some(push_min), Some(pop_min)) => {
                if *push_min < *pop_min {
                    Some(push_min)
                } else {
                    Some(pop_min)
                }
            }
        }
    }

    pub fn pop(&mut self) -> Option<T> {
        if self.popping_stack.is_empty() { self.topple_over(); }
        self.popping_stack.pop()
    }

    fn topple_over(&mut self) {
        while !self.pushing_stack.is_empty() {
            self.popping_stack.push(self.pushing_stack.pop().unwrap());
        }
    }
}


#[cfg(test)]
mod tests {
    use super::MinQueue;

    #[test]
    fn create() {
        let q = MinQueue::<usize>::new();
        assert!(q.is_empty());
    }

    #[test]
    fn min_update_during_push() {
        let mut q = MinQueue::<usize>::new();
        q.push(10);
        assert_eq!(q.min().unwrap(), &10);
        q.push(12);
        assert_eq!(q.min().unwrap(), &10);
        q.push(4);
        assert_eq!(q.min().unwrap(), &4);
        q.push(7);
        assert_eq!(q.min().unwrap(), &4);
        assert!(q.popping_stack.is_empty());
    }

    #[test]
    fn min_update_during_pop() {
        let mut q = MinQueue::<usize>::new();
        q.push(20);
        q.push(15);
        q.push(24);
        q.pop();
        assert!(q.pushing_stack.is_empty());
        q.push(17);
        assert_eq!(q.min().unwrap(), &15);
        q.pop();
        assert_eq!(q.min().unwrap(), &17);
    }
}

